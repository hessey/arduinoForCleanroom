/* 
  This sketch reads the ambient temperature, humidity and differential pressure, prints them to serial output, posts them online and then goes to sleep.
  The values measured are posted through MQTT.
  Don't forget to connect pin 16 with the reset pin for the sleep to end with a wake-up.
  Printing and posting can be enabled/disabled by uncommenting/commenting the relative options in the definitions section below.
  Author: Guescio.
  Simplified/modified by N. Hessey, May 2019.
  Oct 2019: use MAC address as topic, so all canaries use same code.
            and use literal null instead of "NaN"
*/

//******************************************
//definitions

#define SLEEPTIME 60   // seconds
#define PRINTSERIAL    // Print measurements to serial output
#define POST           // Connect and post measurements online
//#define VERBOSE        //print connection status and posting details

#include <limits>
#include <stdio.h>
#include "Adafruit_SHT31.h"
#include <Wire.h>
#include "SDP6x.h"
#ifdef POST
  #include <ESP8266WiFi.h>
  //#include <ESP32.h>
  #include <PubSubClient.h>
  //falkor MQTT
  #define MQTTTOPIC ("cleanroom")
  const char *ssid     = "TRIUMF IoT";
  const char *password = "";
  char clientID[20];
#endif

Adafruit_SHT31 sht31 = Adafruit_SHT31();

//******************************************
//MQTT and wi-fi setup
#ifdef POST
  char MQTTServer[] = "falkor.triumf.ca";
  long MQTTPort = 1883;
  char MQTTUsername[] = "canary";
  char MQTTPassword[] = "TRIUMFITkcleanroom";
  WiFiClient WiFiclient;                // initialize the wifi client library
  PubSubClient MQTTClient(WiFiclient); // initialize PuBSubClient library
#endif //POST

//******************************************
//setup
void setup(){
  Serial.begin(115200);
  while(!Serial) {}
  Serial.println(); // New line in case usb has lots of junk
   
  // Initialize sensors (SDP6x does not need any initialisation)
  sht31.begin();
  int status = sht31.readStatus();
  if (status == 0xFFFF) {
    Serial.print("Bad SHT35 status, no T and RH readings, status is ");
    Serial.println(status, HEX);
  }

  // Read values before the ESP8266 heats up
   
  Wire.begin();                   // Reclame the I2C bus as master
  Wire.setClockStretchLimit(1e4); // µs

  float t = sht31.readTemperature();
  float rh = sht31.readHumidity();
  float dp = SDP6x.GetPressureDiff();

  #ifdef PRINTSERIAL
    printSerial(t, rh, dp);
  #endif //PRINTSERIAL
  
  //connect to wifi and post data
  #ifdef POST
    MQTTClient.setServer(MQTTServer, MQTTPort);
    wifiConnect();
    //post data
    if (WiFi.status() == WL_CONNECTED) {      
      postData(t, rh, dp);
    }
    WiFi.disconnect();
  #endif // POST
  ESP.deepSleep(SLEEPTIME*1e6); // µs
}

//******************************************
//loop() is empty since the ESP8266 is sent to deep sleep at the end of setup()
void loop(){}

//******************************************
//get dew point
float getDewPoint(float t, float rh){
  if (!isnan(t) and !isnan(rh)){
    float h = (log10(rh) - 2) / 0.4343 + (17.62 * t) / (243.12 + t);
    return 243.12*h/(17.62-h);
  }
  else return std::numeric_limits<double>::quiet_NaN();
}

//******************************************
//print measurements to serial output
void printSerial(float t, float rh, float dp){
  Serial.print("t [C], RH [%], DP [C], diff.P [mbar]: ");
  Serial.print(t);
  Serial.print(", ");
  Serial.print(rh);
  Serial.print(", ");
  Serial.print(getDewPoint(t, rh));
  Serial.print(", ");
  if (isnan(dp))
    Serial.println("null"); 
  else
    Serial.println(dp / 100.); // Differential pressure, convert Pa to mbar
}

//******************************************
//connect to wifi
//try connecting for 20 times with 1 s delay
#ifdef POST
void wifiConnect() {
  byte mac[6];
  //https://github.com/esp8266/Arduino/issues/2702
  WiFi.disconnect();
  WiFi.begin(ssid, password);
  #ifdef VERBOSE
    Serial.print("\nwifi connecting to ");
    Serial.println(ssid);
  #endif // VERBOSE
  // Wait for connection for up to 20 seconds
  for (int ii = 0; ii < 20; ii++){
    #ifdef VERBOSE
      Serial.print("status: ");
      switch (WiFi.status()){
        case 0:
          Serial.println("idle");
          break;
        case 1:
          Serial.println("no SSID");
          break;
        case 2:
          Serial.println("scan compl.");
          break;
        case 3:
          Serial.println("connected");
          break;
        case 4:
          Serial.println("connection failed");
          break;
        case 5:
          Serial.println("connection lost");
          break;
        case 6:
          Serial.println("disconnected");
          break;
        default:
          Serial.println("unknown");
          break;
      }
    #endif //VERBOSE
    
    if (WiFi.status() == WL_CONNECTED){
      WiFi.macAddress(mac);
      for (int m = 0; m < 6; ++m) {
        sprintf(&clientID[2 * m], "%02X", mac[m]);
      }

      #ifdef VERBOSE
        Serial.println();
        Serial.print("wifi connected to ");
        Serial.println(ssid);
        Serial.print("IP: ");
        Serial.println(WiFi.localIP());
        Serial.print("RSSI: ");
        Serial.println(WiFi.RSSI());
        Serial.println();
        Serial.print("MAC: ");
        for (int m = 0; m < 6; ++m) {
          Serial.print(mac[m], HEX);
          Serial.print(":");
        }
        Serial.println();
        Serial.print("ClientID is ");
        Serial.print(clientID);
        Serial.println();
      #endif //VERBOSE
      break;
    }
    
    //delay between trials
    delay(1000);//ms
  }

  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("could not connect to ");
    Serial.println(ssid);
    Serial.println();
  }
}
#endif //POST

//******************************************
//post data online using MQTT protocol
#ifdef POST
void postData(float t, float rh, float dp){
  //connect to MQTT broker
  MQTTConnect(clientID);
  
  if (MQTTClient.connected()) {
    //call the loop continuously to establish connection to the server
    MQTTClient.loop();
    MQTTPublish(t, rh, dp);
    MQTTClient.disconnect();
  }
}
#endif //POST

//******************************************
#ifdef POST
void MQTTConnect(char *clientID){

  //show status
  #ifdef VERBOSE
    Serial.print("connecting to MQTT server ");
    Serial.print(MQTTServer);
    Serial.println("...");
  #endif
  
  //try connecting for 5 times
  //using 2 second intervals
  for (int ii = 0; ii < 5; ii++){   
    MQTTClient.connect(clientID, MQTTUsername, MQTTPassword);
    //connection status
    //print to know why the connection failed
    //see http://pubsubclient.knolleary.net/api.html#state for the failure code explanation
    #ifdef VERBOSE
      Serial.print("status: ");
      switch (MQTTClient.state()){
        case -4:
          Serial.println("timeout");
          break;
        case -3:
          Serial.println("lost");
          break;
        case -2:
          Serial.println("failed");
          break;
        case -1:
          Serial.println("disconnected");
          break;
        case 0:
          Serial.println("connected");
          break;
        case 1:
          Serial.println("bad protocol");
          break;
        case 2:
          Serial.println("bad client ID");
          break;
        case 3:
          Serial.println("unavailable");
          break;
        case 4:
          Serial.println("bad credentials");
          break;
        case 5:
          Serial.println("unauthorized");
          break;
        default:
          Serial.println("unknown");
          break;
      }
    #endif
    
    //upon successful connection
    if (MQTTClient.connected()){
      #ifdef VERBOSE
        Serial.print("MQTT connected to ");
        Serial.println(MQTTServer);
      #endif
      break;
    }
    else delay(2000); // ms
  } // END connecting loop
}
#endif //POST


//******************************************
#ifdef POST
void MQTTPublish(float t, float rh, float dp){
float dewp;
  //print
  #ifdef VERBOSE
    Serial.println("posting data");
  #endif

  //create data string to send to MQTT broker
  String data = ("{");
  data += "\"temperature\":";
  data += isnan(t)? "null": String(t);
  data += ",\"relativehumidity\":";
  data += isnan(rh)? "null": String(rh);
  data += ",\"dewpoint\":";
  if (!isnan(t) && !isnan(rh)) {
    dewp = getDewPoint(t,rh);
    data += isnan(dewp)? "null": String(dewp);
  }
  else {
    data += ",\"dewpoint\":null";
  }
  data += ",\"differentialpressure\":";
  data += isnan(dp)? "null": String(dp);

  data += String("}");

  int length = data.length();
  char msgBuffer[length + 1];
  data.toCharArray(msgBuffer, length+1);

  #ifdef VERBOSE
    Serial.print("message: ");
    Serial.println(msgBuffer);
  #endif

  String topicString = MQTTTOPIC;
  topicString += "/";
  topicString += String(clientID);

  length = topicString.length();
  char topicBuffer[length + 1];
  topicString.toCharArray(topicBuffer, length + 1);
  #ifdef VERBOSE
    Serial.print("topic: ");
    Serial.println(topicString);
  #endif
  
  if (MQTTClient.publish(topicBuffer, msgBuffer)) {
    #ifdef VERBOSE
      Serial.println("success");
    #endif
  } 
  else {
    #ifdef VERBOSE
      Serial.println("fail");
    #endif
  }
  Serial.println();

}
#endif //POST
